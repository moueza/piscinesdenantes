import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JourdelasemaineComponent } from './jourdelasemaine.component';

describe('JourdelasemaineComponent', () => {
  let component: JourdelasemaineComponent;
  let fixture: ComponentFixture<JourdelasemaineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JourdelasemaineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JourdelasemaineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
