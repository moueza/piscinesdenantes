import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TableaudestemperaturesComponent } from './tableaudestemperatures/tableaudestemperatures.component';
import { TableaudeshorairesComponent } from './tableaudeshoraires/tableaudeshoraires.component';
import { PeriodeComponent } from './periode/periode.component';
import { JoursComponent } from './jours/jours.component';
import { IntervalhoraireComponent } from './intervalhoraire/intervalhoraire.component';
import { JourneeComponent } from './journee/journee.component';
import { JourdelasemaineComponent } from './jourdelasemaine/jourdelasemaine.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LeftComponent } from './left/left.component';
import { RightComponent } from './right/right.component';
import { InputformComponent } from './inputform/inputform.component';
import { PrintoutputComponent } from './printoutput/printoutput.component';

@NgModule({
  declarations: [
		 AppComponent,
		 TableaudestemperaturesComponent,
		 TableaudeshorairesComponent,
		 PeriodeComponent,
		 JoursComponent,
		 IntervalhoraireComponent,
		 JourneeComponent,
		 JourdelasemaineComponent,
		 HeaderComponent,
		 FooterComponent,
		 LeftComponent,
		 RightComponent,
		 InputformComponent,
		 PrintoutputComponent
		 ],
      imports: [
		BrowserModule,
	    
		
		],
      providers: [],
      bootstrap: [AppComponent]
      })
export class AppModule {
  // Initialize Cloud Firestore through Firebase
  // var db = firebase.firestore();
 // db = AngularFireModule.firestore();
  // console.log(db);


  c1=11;
  d8=888;
}
