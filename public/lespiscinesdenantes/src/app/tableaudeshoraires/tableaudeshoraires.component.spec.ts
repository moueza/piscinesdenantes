import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableaudeshorairesComponent } from './tableaudeshoraires.component';

describe('TableaudeshorairesComponent', () => {
  let component: TableaudeshorairesComponent;
  let fixture: ComponentFixture<TableaudeshorairesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableaudeshorairesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableaudeshorairesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
