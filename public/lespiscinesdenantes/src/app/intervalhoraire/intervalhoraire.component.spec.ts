import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntervalhoraireComponent } from './intervalhoraire.component';

describe('IntervalhoraireComponent', () => {
  let component: IntervalhoraireComponent;
  let fixture: ComponentFixture<IntervalhoraireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntervalhoraireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntervalhoraireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
