import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintoutputComponent } from './printoutput.component';

describe('PrintoutputComponent', () => {
  let component: PrintoutputComponent;
  let fixture: ComponentFixture<PrintoutputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintoutputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintoutputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
