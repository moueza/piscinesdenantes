import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableaudestemperaturesComponent } from './tableaudestemperatures.component';

describe('TableaudestemperaturesComponent', () => {
  let component: TableaudestemperaturesComponent;
  let fixture: ComponentFixture<TableaudestemperaturesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableaudestemperaturesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableaudestemperaturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
